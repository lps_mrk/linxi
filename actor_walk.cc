

namespace gazebo
{
  class actor_walk : public ModelPlugin
  {
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
    // Store the pointer to the model指针指向该模型
      this->actor = boost::dynamic_pointer_cast<physics::Actor>(_parent);
	    this->sdf = _sdf;
	  
	    this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&actor_walk::OnUpdate, this));
	  
	    if(_sdf->HasElement("bvhfile"))
	    {
	  	  //Common::BVHLoader loader;//创建一个BVHLoader类
        Common::Skeleton *bvhskel = nullptr;
   		  bvhskel = loader.Load(_sdf->Get<std::string>("bvhfile"),1.0);
	    }
	  
	    std::map<std::string, std::string> skelMap;
      bool compatible = true;

	    if (this->actor->skeleton->GetNumNodes() != bvhskel->GetNumNodes())
        compatible = false;
      else
      {
        for (unsigned int i = 0; i < this->actor->skeleton->GetNumNodes(); ++i) //遍历所有节点
        {
          SkeletonNode *skinNode = this->actor->skeleton->GetNodeByHandle(i);  //创建可控制的节点
          SkeletonNode *animNode = bvhskel->GetNodeByHandle(i);
        if (animNode->GetChildCount() != skinNode->GetChildCount())  //如果子节点数量不同，则不兼容
        {
          compatible = false;
          break;
        }
        // If compatible, associate the animation node to the skin node 若兼容，将animation和skin相关联
        else
          skelMap[skinNode->GetName()] = animNode->GetName();
        }
      }
	  
	    this->actor->skelAnimation = bvhskl->GetAnimation(0);
	    this->actor->skelNodesMap = skelMap;
	  }

    // Called by the world update start event
    public: void OnUpdate()
    {
      
      this->actor->Load(this->sdf);
    }

    
    // Pointer to the actor 
    private: physics::ActorPtr actor;

    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection;
	
    private: sdf::ElementPtr sdf;

    private: common::BVHLoader loader;
	
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(actor_walk)
}