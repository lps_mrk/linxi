//#ifndef GAZEBO_PLUGINS_ACTOR_PLUGIN_HH_
//#define GAZEBO_PLUGINS_ACTOR_PLUGIN_HH_

#include <string>
#include <vector>

#include <ignition/math/Vector3.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/physics/physics.hh>
#include "gazebo/util/system.hh"

namespace gazebo
{
    class GAZEBO_VISIBLE actor_plugin : public ModelPlugin
    {
        //constructor
        public: actor_plugin();

        //Load the actor 
        //param[in] _model Pointer to the parent model
        //param[in] _sdf Pointer to the plugin's SDF elements
        public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);

        //Documentation Inherited
        public: virtual void Reset();

        //Function that is called every update cycle
        //param[in] _info Timing informationg
        private: void OnUpdate(const common::UpdateInfo &_info);

        //Helper Function to choose a new target location
        private: void ChooseNewTarget();

        //Helper function to avoid obstacles.
        //param[in] _pos Direction vector that should be adjusted according
        //to nearby obstacles
        private: void HandleObstacles(ignition::math::vector3d &_pos);

        //Pointer to the parent actor
        private: physics::ActorPtr actor;

        //Pointer to the world
        private: physics::WorldPtr world;

        //Pointer to the sdf element.
        private: sdf::ElementPtr sdf;

        // Velocisy of the actor
        private: ignition::math::Vector3d velocity;

        //List of connections;
        private: std::vector<event::ConnectionPtr> connections;

        //Current target location
        private: ignition::math::Vector 3d target;

        //Targe location weit
        private: double targetWeight = 1.0;

        //Obstacle weight
        private: double obstacleWeight =1.0;

        //Time scaling factor. 
        private: double animationFactor =1.0;

        //Time of the last update
        private: common::Time lastUpdate;

        //List of models to ignore.
        private: std::vector<std::string> ignoreModels;

        //Custon trajectory info
        private: pyhsics::TrajectoryInfoPtr trajectoryInfo;


    };
} // namespace gazebo
//#endif