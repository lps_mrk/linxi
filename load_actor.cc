#include <functional>
#include <string>
#include <vector>
#include <ignition/math.hh>
#include "ignition/math/Vector3.hh"
#include "gazebo/common/Plugin.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/util/system.hh"
#include "iostream"
#include <ignition/math/Quaternion.hh>
#include <ignition/math/Vector3.hh>

#include "gazebo/common/Animation.hh"
#include "gazebo/common/CommonTypes.hh"
#include "gazebo/common/KeyFrame.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/physics/Model.hh"

using namespace std;



namespace gazebo
{    
    class load_actor : public ModelPlugin
    {
        public: void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
        {
			this->actor = boost::dynamic_pointer_cast<physics::Actor>(_model); //get the parent element
            this->world = this ->actor->GetWorld();
			
			auto skinSdf = _sdf->GetParent()->GetElement("skin");
			skinSdf->GetElement("filename")->Set("/home/linxi/Human_dae/stand.dae");
			

			auto animSdf = _sdf->GetParent()->GetElement("animation");
			animSdf->GetAttribute("name")->Set("walking");
			animSdf->GetElement("filename")->Set("/home/linxi/Human_dae/walk.dae");
			animSdf->GetElement("interpolate_x")->Set("true");

			animSdf = _sdf->GetParent()->AddElement("animation");
			animSdf->GetAttribute("name")->Set("sit_down");
			animSdf->GetElement("filename")->Set("/home/linxi/Human_dae/sit_down.dae");
			
			
			auto scriptSdf = _sdf->GetParent()->GetElement("script");
			auto trajSdf = scriptSdf->GetElement("trajectory");
			trajSdf->GetAttribute("id")->Set("0");
			trajSdf->GetAttribute("type")->Set("walking");
			auto waypSdf = trajSdf->AddElement("waypoint");
			waypSdf->GetElement("time")->Set(0);
			waypSdf->GetElement("pose")->Set("0 2 0 0 0 -1.57");
			
			waypSdf = trajSdf->AddElement("waypoint");
			waypSdf->GetElement("time")->Set(5);
			waypSdf->GetElement("pose")->Set("0 -5 0 0 0 -1.57");

//////////////////////////////////////////////////////////////////////////
			
			trajSdf = scriptSdf->AddElement("trajectory");
			trajSdf->GetAttribute("id")->Set("1");
			trajSdf->GetAttribute("type")->Set("sit_down");
			waypSdf = trajSdf->AddElement("waypoint");
			waypSdf->GetElement("time")->Set(5);
			waypSdf->GetElement("pose")->Set("0 -5 0 0 0 -1.57");

			
			
  			this->actor->Load(_sdf->GetParent());
			
        }

        private: physics::ActorPtr actor;
        private: physics::WorldPtr world;
       
    };
    GZ_REGISTER_MODEL_PLUGIN(load_actor);
}